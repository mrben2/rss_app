<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'CodingMs.RssApp',
            'RssApp',
            ['RssApp' => 'show'],
            ['RssApp' => '']
        );
        //
        $addPageTSConfig = '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:rss_app/Configuration/PageTS/tsconfig.typoscript">';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig($addPageTSConfig);
        //
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1585186052] = [
            'nodeName' => 'RssAppRow',
            'priority' => '70',
            'class' => \CodingMs\RssApp\Form\Element\RssAppRow::class,
        ];
    }
);

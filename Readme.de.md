# RSS-App-Integration für TYPO3

Mit diesem Plugin können Sie RSS-App-Feeds in TYPO3 anzeigen. Die RSS-App (https://rss.app/) bietet viele verschiedene RSS-Feeds von Plattformen wie:

Instagram, Twitter, Google News, Reddit, Facebook, YouTube, Telegramm, Pinterest, eBay, Tumblr, Imgur, Giphy, Unsplash, Scoop.it, NYTimes, CNN, USA Today, BBC, NPR, Washington Post, Vogue, ELLE, Harper's BAZAAR, Vanity Fair, Allure, WWD, Wired, MIT Technology Review, Glamour, InStyle, GQ, Unternehmer, Fast Company, Fortune

Sie können das Plugin auch verwenden, um RSS-Feeds aus anderen Quellen auf Ihrer Website anzuzeigen.

## Installation

1. Installieren Sie die Extension mittels composer (```composer req codingms/rss-app```) or dem Extension-Manager.
2. Fügen Sie die statischen Templates der Erweiterung ihrem Basis-Template hinzu.
3. Fügen Sie das Plugin "RssApp" auf den Seiten ein, auf denen Sie RSS-Feeds anzeigen möchten.



## Support

Für kostenpflichtigen Support senden Sie eine E-Mail an typo3@coding.ms

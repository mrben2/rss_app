<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {
        //
        // Plugins
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'CodingMs.rss_app',
            'RssApp',
            'RssApp'
        );
        //
        // Include flex forms
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['rssapp_rssapp'] = 'pi_flexform';
        $flexForm = 'FILE:EXT:rss_app/Configuration/FlexForms/RssApp.xml';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('rssapp_rssapp', $flexForm);
        //
        // Static template
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('rss_app', 'Configuration/TypoScript', 'RSS-App');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('rss_app', 'Configuration/TypoScript/OwlCarousel', 'RSS-App - OwlCarousel');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('rss_app', 'Configuration/TypoScript/Stylesheets', 'RSS-App - Default stylesheets');
        //
        // register svg icons: identifier and filename
        $iconsSvg = [
            'content-plugin-rssapp-rssapp' => 'ext_icon.svg',
        ];
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
        foreach ($iconsSvg as $identifier => $path) {
            $iconRegistry->registerIcon(
                $identifier,
                \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                ['source' => 'EXT:rss_app/' . $path]
            );
        }
    }
);

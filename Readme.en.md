# RSS-App integration in TYPO3

This Plugin enables you to display RSS-App feeds in TYPO3. RSS-App (https://rss.app/) provides a lot of different RSS-Feeds from platforms such as:

Instagram, Twitter, Google News, Reddit, Facebook, YouTube, Telegram, Pinterest, eBay, Tumblr, imgur, Giphy, Unsplash, Scoop.it, NYTimes, CNN, USA Today, BBC, NPR, Washington Post, Vogue, ELLE, Harper's BAZAAR, Vanity Fair, Allure, WWD, Wired, MIT Technology Review, Glamour, InStyle, GQ, Entrepreneur, Fast Company, Fortune

You can also use this extension to display feeds from other sources on your website.


## Installation

1. Install the extension using composer (```composer req codingms/rss-app```) or the extension manager.
2. Include the static templates of the extension in your base template.
3. Insert the plugin "RssApp" where you want to display the RSS feed.

## Support

For paid support please send an email to typo3@coding.ms

<?php
namespace CodingMs\RssApp\Form\Element;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Backend\Form\Element\AbstractFormElement;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Package\Exception;

/**
 * Displays a row of information about RSS-App
 *
 * @package rss_app
 * @subpackage Form
 * @author Thomas Deuling <typo3@coding.ms>
 *
 * @version 1.0.0
 */
class RssAppRow extends AbstractFormElement
{

    /**
     *
     * @return array
     * @throws Exception
     * @throws ExtensionConfigurationExtensionNotConfiguredException
     * @throws ExtensionConfigurationPathDoesNotExistException
     */
    public function render()
    {
        $result['html'] = '<a href="https://rss.app/" target="_blank"><u>RSS.app</u></a> is a service which provides you RSS feeds from your prefered sources. You can easily create feed URLs for Instagram, Twitter, Facebook and much more. Click <a href="https://rss.app/rss-feed" target="_blank"><u>here</u></a> for create a new feed!';
        return $result;
    }

}

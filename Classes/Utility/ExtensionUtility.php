<?php
namespace CodingMs\RssApp\Utility;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Extension utilities
 *
 * @package rss_app
 * @subpackage Utility
 *
 * ChangeLog:
 * 2019-01-19
 */
class ExtensionUtility
{

    /**
     * @param string $extensionKey
     * @return array|mixed
     */
    public static function getExtensionConfiguration(string $extensionKey) {
        $configuration = [];
        if((int)TYPO3_version >= 9) {
            // Attention: Full namespace required!
            $configuration = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get($extensionKey);
        }
        if((int)TYPO3_version === 8) {
            if(isset($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$extensionKey])) {
                $configuration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$extensionKey]);
                // Attention: Full namespace required!
                $typoScriptService = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Service\TypoScriptService::class);
                $configuration = $typoScriptService->convertTypoScriptArrayToPlainArray($configuration);
            }
        }
        return $configuration;
    }

}

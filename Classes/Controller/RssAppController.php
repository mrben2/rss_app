<?php

namespace CodingMs\RssApp\Controller;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\RssApp\Service\RssAppService;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use Exception;

/**
 * RSS-App Controller
 * @noinspection PhpUnused
 */
class RssAppController extends ActionController
{

    /**
     * @var RssAppService
     */
    protected $rssAppService;

    /**
     * @param RssAppService $rssAppService
     * @noinspection PhpUnused
     */
    public function injectRssAppService(RssAppService $rssAppService)
    {
        $this->rssAppService = $rssAppService;
    }

    /**
     * @throws Exception
     */
    protected function initializeAction()
    {
        parent::initializeAction();
    }

    /**
     * Show RSS-App feed
     *
     * @return void
     * @noinspection PhpUnused
     */
    public function showAction()
    {
        $feed = [];
        try {
            $feed = $this->rssAppService->getData($this->settings);
        } catch (Exception $exception) {
            $this->addFlashMessage($exception->getMessage(), 'Error', AbstractMessage::ERROR);
        }
        $this->view->assign('feed', $feed);
    }

}

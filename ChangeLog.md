# RSS-App Change-Log

## 2021-05-15 Release of version 1.0.6

*   [TASK] Add extension key in composer.json



## 2020-12-15 Release of version 1.0.5

*   [TASK] Add translation files see #1
*   [TASK] Check if node properties exist before accessing them
*   [TASK] Add note on installation in the readme files



## 2020-10-04 Release of version 1.0.4

*   [TASK] Add extra tags in composer.json



## 2020-10-04 Release of version 1.0.3

*   [BUGFIX] Rise additional_tca requirement



## 2020-08-20 Release of Version 1.0.2

*	[TASK] Add extra tags in composer.json
*   [TASK] Add additional_tca support



## 2020-06-17 Release of Version 1.0.1

*   [BUGFIX] Fix items without images



## 2020-06-14 Release of Version 1.0.0

*   [TASK] Implement base features
